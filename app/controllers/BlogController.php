<?php

class BlogController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
//        $blog = InformacionBlog::first();
        $posts = Post::all();
        return View::make('index', array(
                    'posts' => $posts,
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        //
//        $blog = InformacionBlog::first();
        return View::make('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        //
        $data = Input::all();
        Input::flashOnly('title', 'subTitle', 'body', 'tags'); //ni idea, pero lo uso para que withInput no de error
//        echo json_encode($data);
        $tags = $data['tags'];
        $tags = str_replace(' ', '', $tags); //borrando el espacio ¬¬
        $tags = explode(',', $tags); // separando por ',' los tags
        $tags_id = array(); // en esta variable guardare todos los id del tags
//        echo json_encode($tags);
        foreach ($tags as $tag) {
            array_push($tags_id, Tag::firstOrCreate(array('name' => $tag))->id);
        }
//        echo json_encode($data);
        $post = new Post();
        $data_post  = array(
            'title' => $data['title'],
            'subTitle' => $data['subTitle'],
            'body' => $data['body'],
            'url' => Str::slug($data['title'])
        );
        if ($post->validate($data_post)) {

//            echo json_encode($data);

            $post = Post::create([
                        'title' => $data_post['title'],
                        'subTitle' => $data_post['subTitle'],
                        'body' => $data_post['body'],
                        'user_id' => '1',
                        'url' => Str::slug($data_post['title'])
            ]);


            $post->tags()->sync($tags_id);
            Session::flash('message', 'New Post create');
            Session::flash('alert', 'alert-success');
            return Redirect::to('blog');
        } else {
            $errors = $post->errors();
            Session::flash('message', 'Validate input');
            Session::flash('alert', 'alert-danger');
            return View::make('create')
                            ->withInput(Input::all())
                            ->withErrors($errors);
//            echo json_encode($error);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($post_url) {
        //
//        $blog = InformacionBlog::first(); 
//        $post = Post::find($id);
        $post = Post::where('url', '=', $post_url)->first();
        if ($post != null) {
            return View::make('showpost', array(
                        'post' => $post,
            ));
        } else {
            Session::flash('message', 'No found post');
            Session::flash('alert', 'alert-danger');
            return Redirect::to('blog');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
