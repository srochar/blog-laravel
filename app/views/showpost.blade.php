@extends('layout/master')

@section('conntet')
<div class="row">

    <div class="col-sm-8 blog-main">

        <div class="blog-post">
            @include('layout/headpost')
            {{$post->body}}
            <p class="blog-post-meta">Tags: @foreach ($post->tags as $tag) <a href="#">{{$tag->name}}</a> @endforeach </p>
            <hr>
        </div><!-- /.blog-post -->
        <h2>Comment</h2>
        @foreach ($post->comments as $comment)
        <div class="blog-comment">
            <div class="blog-name" >
                <h4 >{{$comment->name}} on {{$comment->created_at}} says: </h4>
                <p class="text-info">{{$comment->mail}} - {{$comment->ip}}</p>
                <p class="text-muted">{{$comment->body}}</p>
            </div>
        </div>
        <hr>
        @endforeach

    </div><!-- /.blog-main -->


    @include('layout/sidebar')

</div><!-- /.row -->
@stop