@extends('layout/master')

@section('conntet')
<div class="row">

    <div class="col-sm-8 blog-main">

        @foreach ($posts as $post)
        <div class="blog-post">
            @include('layout/headpost')
            {{ $post->body }}
            <p class="blog-post-meta">
                @include('layout/tagspost')
                ,<span class="glyphicon glyphicon-comment" aria-hidden="true"></span> Comments: {{$post->comments()->count()}}
            </p>


        </div><!-- /
        </div><!-- /.blog-post -->

        <hr>
        @endforeach
        <nav>
            <ul class="pager">
                <li><a href="#">Previous</a></li>
                <li><a href="#">Next</a></li>
            </ul>
        </nav>

    </div><!-- /.blog-main -->


    @include('layout/sidebar')

</div><!-- /.row -->
@stop