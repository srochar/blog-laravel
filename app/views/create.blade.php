@extends('layout/master')

@section('conntet')
    <h2>New Post</h2>
    <hr>
    <div class="row">

        <div class="col-sm-12 blog-main">

<!--            @if ($errors->has())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                {{ $error }}<br>        
                @endforeach
            </div>
            @endif-->

            {{Form::open(array('url' => 'blog', 'class' => 'form-horizontal'))}}

            <div class="form-group {{ ($errors and $errors->has('title') or $errors->has('url') ) ? 'has-error' : '' }}">
                <label for="inputEmail3" class="col-sm-2 control-label">Title:</label>
                <div class="col-sm-10">
                    <!--<input type="text" name="title" class="form-control" id="inputEmail3" placeholder="Laravel 5">-->
                    {{Form::text('title','',array('placeholder'=>'Laravel 5','class'=>'form-control'))}}
                    @if ($errors->has('title')) <label class="control-label">{{ $errors->first('title') }}</label> @endif
                    @if ($errors->has('url')) <label class="control-label">{{ $errors->first('url') }}</label> @endif
                </div>
            </div>
            <div class="form-group {{ ($errors and $errors->has('subTitle')) ? 'has-error' : '' }}">
                <label for="inputPassword3" class="col-sm-2 control-label">SubTitle:</label>
                <div class="col-sm-10">
                    <!--<input type="text" name="subtitle" class="form-control" id="inputPassword3" placeholder="New version 2015">-->
                    {{Form::text('subTitle','',array('placeholder'=>'New version 2015','class'=>'form-control'))}}
                    @if ($errors->has('subTitle')) <label class="control-label">{{ $errors->first('subTitle') }}</label> @endif
                </div>
            </div>
            <div class="form-group {{ ($errors and $errors->has('body')) ? 'has-error' : '' }} ">
                <label for="inputPassword3" class="col-sm-2 control-label">Body:</label>
                <div class="col-sm-10">
                    <!--<textarea class="form-control" name="body" rows="10" placeholder="Laravel 5 is.."></textarea>-->
                    {{ Form::textarea('body', '',array('rows'=>'10','placeholder'=>'Laravel 5 is...','class'=>'form-control')) }}
                    @if ($errors->has('body')) <label class="control-label">{{ $errors->first('body') }}</label> @endif
                </div>
            </div>
            <div class="form-group ">
                <label for="inputPassword3" class="col-sm-2 control-label">Tags:</label>
                <div class="col-sm-10">
                    {{Form::text('tags','',array('placeholder'=>'linux,php,laravel','class'=>'form-control'))}}
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Autor:</label>
                <div class="col-sm-10">
                    <input class="form-control" id="disabledInput" type="text" placeholder="Srocha" disabled>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default">Publish</button>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div><!-- /.row -->

</div><!-- /.container -->
@stop