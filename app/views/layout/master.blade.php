<!DOCTYPE html>
<html lang="es">

    @include('layout/head')

    <body>
        @include('layout/nav')

        <div class="container">

            <div class="blog-header">
                <h1 class="blog-title">{{$blog->title}}</h1>
                <p class="lead blog-description">{{$blog->subTitle}}</p>
            </div>
            @if(Session::has('message'))
            <div class="alert {{Session::get('alert')}} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Srocha!</strong> {{ Session::get('message') }}
            </div>
            @endif
            
            @section('conntet')
                No found
            @show

            @include('layout/footer')

    </body>


</html>
