<a href="{{URL::route('blog.edit',$post->url)}}" ><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
    {{HTML::linkRoute('blog.show',$post->title,$post->url,array('class'=>'h1') )}} 
<p class="lead blog-description">
    {{$post->subTitle}}
</p>
<p class="blog-post-meta">
    <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
    {{$post->getDateCreated_at()}} by <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
    <a href="#">{{$post->user->name}}</a>
</p>