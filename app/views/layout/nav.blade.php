<div class="blog-masthead">
    <div class="container">
        <nav class="blog-nav">
            {{HTML::linkRoute('blog.index','Home',array(),array('class'=>'blog-nav-item'))}}
            {{HTML::linkRoute('blog.create','New Post',array(),array('class'=>'blog-nav-item'))}}
        </nav>
    </div>
</div>