<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Tag extends \Eloquent{
    
    protected $table = 'tags';
    protected $fillable = array('name');


    public function posts() {
        return $this->belongsToMany('Post','tags_post','tag_id','post_id')->withTimestamps();
    }
    
}