<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Post extends \Eloquent {

    protected $table = 'posts';
    protected $fillable = array('title', 'subTitle', 'body', 'user_id', 'url');

    public function comments() {
        return $this->hasmany('Comment');
    }

    public function tags() {
        return $this->belongsToMany('Tag', 'tags_posts', 'post_id', 'tag_id')->withTimestamps();
    }

    public function user() {
        return $this->belongsTo('User');
    }

    public function getDateCreated_at() {
        $date = new DateTime($this->attributes['created_at']);
        return $date->format('l d M Y');
    }

    private $rules = array(
        'title' => 'required|min:4',
        'subTitle' => 'required|min:5',
        'body' => 'required|min:5',
        'url' => 'unique:posts'
    );
    protected $errors;

    public function validate($data) {
        $validation = false;
        // make a new validator object
        $v = Validator::make($data, $this->rules);
//        echo 'asd';
        // check for failure
//        echo json_encode($v);
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v;
            $validation = false;
        } else {
            $validation = true;
        }
        // validation pass
        return $validation;
    }

    public function errors() {
        return $this->errors;
    }

}
