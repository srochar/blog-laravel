<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PostsSeeder extends \Illuminate\Database\Seeder{
    
    public function run(){
        $title = 'Nuevo Titulo';
        $subTitle = 'Sub Titulo';
        $body = 'Cuerpo del posts';
        $user_id = '1';
        Post::create(
            [
                'title' => $title,
                'subTitle' => $subTitle,
                'body' => $body,
                'user_id' => $user_id,
                'url' => Str::slug($title)
            ]
        );
    }
}