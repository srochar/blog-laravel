<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CommentsSeeder extends \Illuminate\Database\Seeder{
    
    public function run(){
        $name = 'Seba';
        $mail = 'srocha@ceinf.cl';
        $ip = '192.168.1.1';
        $body = 'Esto es un comentario';
        $post_id = '1';
        Comment::create(
            [
                'name' => $name,
                'mail' => $mail,
                'ip' => $ip,
                'body' => $body,
                'post_id' => $post_id
            ]
        );
    }
}