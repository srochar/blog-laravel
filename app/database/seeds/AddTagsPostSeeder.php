<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AddTagsPostSeeder extends \Illuminate\Database\Seeder{
    
    public function run(){
        $post_id = '1';
        $tag_id = '1';
        
        $post = Post::find($post_id);
        $tag = Post::find($tag_id);
        
        $post->tags()->attach($tag);
        
    }
}