<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class InformationsSeeder extends \Illuminate\Database\Seeder{
    
    public function run(){
        $titulo = 'DeTodoUnSeba';
        $subTitulo = 'Aportante de un Seba, para el Mundo';
        InformacionBlog::create(
            [
                'title' => $titulo,
                'subTitle' => $subTitulo
            ]
        );
    }
}